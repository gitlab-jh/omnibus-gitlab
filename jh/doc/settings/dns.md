---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# DNS 配置

<!--While it is possible to run a GitLab instance using only IP addresses, it is often beneficial to use DNS as it is easier for users and is required for HTTPS. Depending on the features you want to take advantage of, multiple DNS entries may be necessary. Any of these DNS entries should be of type A, AAAA, or CNAME. This depends on the underlying architecture of the instance you are using.-->

运行 GitLab 实例可只使用 IP 地址，但使用 DNS 解析往往更具优势，不但便于用户使用，而且对于启动 HTTPS 是必需的。根据实际业务需要，可设置多条 DNS 记录。任何 DNS 记录应属于 A、AAAA 或 CNAME 类型，具体要使用的类型取决于实例运行的底层架构。

<!--If you don't want to take advantage of the [Let's Encrypt integration](ssl.md#lets-encrypt-integration),
none of these addresses need to be resolvable over the public internet. Only nodes that
will access the GitLab instance need to be able to resolve the addresses.-->

如果您不需要集成 [Let's Encrypt integration](ssl.md#lets-encrypt-integration)，所有地址都不需要在公共互联网上解析。只有将要访问 GitLab 实例的节点，需要能够解析地址。

<!--Adding these entries to your domain's DNS configuration is entirely dependent on your chosen provider, and out of scope for this document. Consult the documentation from your domain name registrar, hosting provider, or managed DNS provider for the most accurate guidance. Instructions for common DNS registrars include:-->

添加 DNS 记录到您的域名 DNS 设置，完全依赖于您选择的域名提供商，不在本文说明范围内。请从您的域名注册商、托管提供商或托管 DNS 提供商处查阅文档，以获取最准确的指导。常见注册商信息如下：

- [Godaddy](https://www.godaddy.com/help/create-a-subdomain-4080)
- [Namecheap](https://www.namecheap.com/support/knowledgebase/article.aspx/9776/2237/how-to-create-a-subdomain-for-my-domain/)
- [Gandi](https://docs.gandi.net/en/domain_names/faq/dns_records.html)
- [Dreamhost](https://help.dreamhost.com/hc/en-us/articles/214694348-Basic-DNS-records)

<!--It is also possible to utilize dynamic DNS services, such as [nip.io](https://nip.io), for a quick and easy DNS name for non-production instances. We do not recommend these for any production or long-lived instances, as they are often [rate-limited](https://letsencrypt.org/docs/rate-limits/) by Let's Encrypt and [insecure](https://github.com/publicsuffix/list/issues/335#issuecomment-261825647). Even if you are successful on initial registration, renewals may subsequently fail.-->

您也可以使用动态 DNS 服务，例如 [nip.io](https://nip.io)，简单快速地为非生产实例提供 DNS 解析名。不推荐在生产实例或长期使用实例上应用，因为通常会被 Let's Encrypt 和 [insecure](https://github.com/publicsuffix/list/issues/335#issuecomment-261825647) 限制（[rate-limited](https://letsencrypt.org/docs/rate-limits/)）。即使您首次注册成功，续订可能会失败。

## GitLab 设置

<!--Below is a list of attributes for `/etc/gitlab/gitlab.rb` that can take advantage of a corresponding DNS entry.

While it is possible to replace the below DNS entries with a wildcard entry in DNS, you still need to provide your GitLab instance with the individual records, and this will **not** result in the Let's Encrypt integration fetching a wildcard certificate.-->

`/etc/gitlab/gitlab.rb` 可以利用相应的 DNS 条目，以下是其属性列表。

虽然可以使用通配符替换下面的 DNS 记录，但您仍然需要为 GitLab 实例提供独立的记录，并且这**不会**导致 Let's Encrypt 集成获取通配符证书。

### `external_url`

<!--This will be the address that will be used to interact with the main GitLab instance. Cloning over SSH/HTTP/HTTPS will use this address. Accessing the web UI will reference this DNS entry. If you are using a GitLab Runner, it will use this address to talk to the instance.-->

用于与主 GitLab 实例进行交互的地址。通过 SSH/HTTP/HTTPS 克隆时使用该地址。访问 web UI 将引用此 DNS 条目。如果您使用的是 GitLab Runner，它将使用该地址与实例通信。

### `registry_external_url`

<!--If you want to use the [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html), this will be an address that is used to interact with the registry. This can also use the same DNS entry as [external_url](#external_url), on a different port. Can be used by the Let's Encrypt integration.-->

使用容器镜像仓库时，该地址用于镜像仓库交互。该地址可以与 [external_url](#external_url) 相同，使用不同的端口。可以用于 Let's Encrypt 集成。

### `mattermost_external_url`

<!--This is needed if you want to use the [bundled Mattermost](../gitlab-mattermost/README.md) software. Can be used by the Let's Encrypt integration.-->

使用 bundled Mattermost 时需要配置，可以用于 Let's Encrypt 集成。

### `pages_external_url`

<!--By default, projects that use [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html) will deploy to a sub-domain of this value.-->

默认情况下，使用 GitLab Pages 的项目将会部署到该地址的子域名。

### Auto DevOps domain

<!--If you are going to be deploying projects via Auto DevOps with GitLab, this domain can be used to deploy software. Can be defined at an instance, or cluster level. See the [specific documentation](https://docs.gitlab.com/ee/topics/autodevops/#auto-devops-base-domain) for more details.-->

如果您计划通过极狐GitLab 的 Auto DevOps 功能部署项目，Auto DevOps 域名可用于部署软件。该域名可以在实例级别或集群级别上定义。

## 故障排查

<!--If you are having issues accessing a particular component, or if Let's Encrypt integration is failing, you may have a DNS issue. You can use the [dig](https://en.wikipedia.org/wiki/Dig_(command)) tool to check and verify if DNS is causing you a problem.-->

如果您访问特定组件时发生问题，或 Let's Encrypt 集成失效，您可能遇到 DNS 问题。您可以使用 [dig](https://en.wikipedia.org/wiki/Dig_(command)) 工具，检查确认是否是 DNS 导致了您的问题。

### 成功的 DNS 查询

```shell
$ dig registry.gitlab.com

; <<>> DiG 9.10.6 <<>> registry.gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 12967
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;registry.gitlab.com.           IN      A

;; ANSWER SECTION:
registry.gitlab.com.    300     IN      A       35.227.35.254

;; Query time: 56 msec
;; SERVER: 172.16.0.1#53(172.16.0.1)
;; WHEN: Fri Mar 20 14:31:24 CDT 2020
;; MSG SIZE  rcvd: 83
```

<!--At the least, you are looking for the status to be `NOERROR`, and the`ANSWER SECTION` for the actual results.-->

最终，您应该可以看到状态为 `NOERROR`，并且 `ANSWER SECTION` 有实际结果。

### 失败的 DNS 查询

```shell
$ dig fake.gitlab.com

; <<>> DiG 9.10.6 <<>> fake.gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 50688
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;fake.gitlab.com.               IN      A

;; AUTHORITY SECTION:
gitlab.com.             900     IN      SOA     ns-705.awsdns-24.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400

;; Query time: 101 msec
;; SERVER: 172.16.0.1#53(172.16.0.1)
;; WHEN: Fri Mar 20 14:51:58 CDT 2020
```

<!--Notice here, that the `status` is `NXDOMAIN`, and there is no `ANSWER SECTION`. The `SERVER` field tells you which DNS server was queried for the answer. By default, this is the primary DNS server used by the station the dig command was run from.-->

这里需要注意，`status` 为 `NXDOMAIN`，`ANSWER SECTION` 没有结果。`SERVER` 展示查询结果的 DNS 服务器。默认情况下，是运行 dig 命令的站点使用的主要 DNS 服务器。
