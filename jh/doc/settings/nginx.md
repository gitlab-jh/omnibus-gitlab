---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# NGINX 配置

## 特定服务 NGINX 配置

<!--Users can configure NGINX settings differently for different services via
`gitlab.rb`. Settings for the GitLab Rails application can be configured using the
`nginx['<some setting>']` keys. There are similar keys for other services like
`pages_nginx`, `mattermost_nginx` and `registry_nginx`. All the configurations
available for `nginx` are also available for these `<service_nginx>` settings and
share the same default values as GitLab NGINX.-->

使用者可以通过 `gitlab.rb`，为不同的服务进行不同的 NGINX 配置。使用 `nginx['<some setting>']` 键可以对 GitLab Rails 应用进行配置。其它服务也有类似的键，例如` pages_nginx`, `mattermost_nginx` 和 `registry_nginx`。所有 `nginx` 的可用配置也同样适用于这些服务的 `<service_nginx>` 配置，并且与 GitLab NGINX 使用相同的默认值。

<!--If modifying via `gitlab.rb`, users have to configure NGINX setting for each
service separately. Settings given via `nginx['foo']` WILL NOT be replicated to
service specific NGINX configuration (as `registry_nginx['foo']` or
`mattermost_nginx['foo']`, etc.). For example, to configure HTTP to HTTPS
redirection for GitLab, Mattermost and Registry, the following settings should
be added to `gitlab.rb`-->

修改 `gitlab.rb` 时，用户需要为每个服务分别进行 NGINX 配置。`nginx['foo']` 的配置**不会**复制到服务特定的 NGINX 配置（例如 `registry_nginx['foo']` 或 `mattermost_nginx['foo']` 等等）。例如，要为 GitLab、Mattermost 和 Registry 配置 HTTP 到 HTTPS 的重定向，以下配置应添加到 `gitlab.rb`。

```ruby
nginx['redirect_http_to_https'] = true
registry_nginx['redirect_http_to_https'] = true
mattermost_nginx['redirect_http_to_https'] = true
```

<!--NOTE:
Modifying NGINX configuration should be done with care as incorrect
or incompatible configuration may yield to unavailability of service.-->

NOTE:
修改 NGINX 配置时应小心谨慎，不正确或不兼容的配置可能会导致服务不可用。

## 启用 HTTPS


默认情况下，Omnibus GitLab 不使用 HTTPS。如果您想为 `gitlab.example.com` 启用 HTTPS，有以下两个选项。

1. [使用 Let's Encrypt 免费和自动化的 HTTPS](ssl.md#lets-encrypt-integration)
1. [使用您自己的证书手动配置 HTTPS](#手动配置 HTTPS)

NOTE:
如果您使用代理，负载平衡器或其他一些外部设备来终止 GitLab 主机名的SSL，请参考 [外部设备、代理和负载均衡器的 SSL 终止](#external-proxy-and-load-balancer-ssl-termination)。

### 警告


NGINX 配置将使浏览器和客户端在接下来的 365 天，使用 [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) 与 GitLab 实例进行安全连接。有关更多配置选项，查看 [设置 HSTS 功能](#setting-http-strict-transport-security)。启用 HTTPS ，您需要在接下来的至少 24 个月，为您的实例提供安全连接。

## 手动配置 HTTPS

默认情况下，Omnibus GitLab 不使用 HTTPS。

为域 `gitlab.example.com` 启用 HTTPS 的步骤如下：

1. 在 `/etc/gitlab/gitlab.rb` 中编辑 `external_url`：

   ```ruby
   # note the 'https' below
   external_url "https://gitlab.example.com"
   ```

1. 在 `/etc/gitlab/gitlab.rb` 中禁用 Let's Encrypt。

   ```ruby
   letsencrypt['enable'] = false
   ```

1. 创建 `/etc/gitlab/ssl` 目录，并将密钥和证书复制到其中：

   ```shell
   sudo mkdir -p /etc/gitlab/ssl
   sudo chmod 755 /etc/gitlab/ssl
   sudo cp gitlab.example.com.key gitlab.example.com.crt /etc/gitlab/ssl/
   ```
   
   由于本例中的主机名为 `gitlab.example.com`，Omnibus GitLab 会分别查找名为 `/etc/gitlab/ssl/gitlab.example.com.key` 的私钥，以及名为 
`/etc/gitlab/ssl/gitlab.example.com.crt` 的公共证书文件。

	确保使用完整的证书链，以免在客户端连接时发生 SSL 错误。完整的证书链顺序应以服务器证书为先，然后是所有中间证书，最后是根 CA。
	
	如果 `certificate.key` 文件受密码保护，NGINX 不会在您重新配置 GitLab 时要求输入密码。在这种情况下，Omnibus GitLab 会失败且无错误消息。要从密钥中移除密码，运行以下命令：

   ```shell
   openssl rsa -in certificate_before.key -out certificate_after.key
   ```

1. 现在，重新配置 GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

当重新配置完成时，您可以通过 `https://gitlab.example.com` 访问您的实例。

如果您使用防火墙，您需要打开 443 端口，才能允许 HTTPS 流量进入。

```shell
# UFW example (Debian, Ubuntu)
sudo ufw allow https

# lokkit example (RedHat, CentOS 6)
sudo lokkit -s https

# firewall-cmd (RedHat, Centos 7)
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld
```

## 重定向 `HTTP` 请求到 `HTTPS`


默认情况下，您指定的 `external_url` 以 'https' 开头时，NGINX 将不会再在 80 端口监听未加密的 HTTP 流量。如果您想要将所有 HTTP 流量重定向到 HTTPS，您可以使用 `redirect_http_to_https` 设置。

NOTE:
默认情况下启用该操作。

```ruby
external_url "https://gitlab.example.com"
nginx['redirect_http_to_https'] = true
```

## 更改默认端口和 SSL 证书位置


如果您需要使用默认端口（443）以外的 HTTPS 端口，只需将其作为 `external_url` 的一部分而指定。

```ruby
external_url "https://gitlab.example.com:2443"
```

要设置 ssl 证书的位置，创建 `/etc/gitlab/ssl` 目录，放置 `.crt` 和 `.key` 文件，并指定以下配置：

```ruby
# For GitLab
nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.example.com.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.example.com.key"
```

运行 `sudo gitlab-ctl reconfigure` 命令使变更生效。

## 更新 SSL 证书


如果您的 SSL 证书内容已更新，但 `gitlab.rb` 的配置没有更新，这样运行 `gitlab-ctl reconfigure` 不会影响 NGINX。运行 `sudo gitlab-ctl hup nginx`，使 NGINX 优雅地[重新加载已有配置和新证书](http://nginx.org/en/docs/control.html)。

## 更改默认的 proxy headers

<!--By default, when you specify `external_url` Omnibus GitLab will set a few
NGINX proxy headers that are assumed to be sane in most environments.

For example, Omnibus GitLab will set:-->

默认情况下，当您指定了 `external_url`，Omnibus GitLab 在大多数环境中设置了一些合理的 NGINX proxy headers。

```plaintext
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
```

当您已在 `external_url` 中指定 `https` 模式。

然而，如果您的实例位于更复杂的设置之中，例如在反向代理之后，您需要调整 proxy headers，以免发生例如 `The change you wanted was rejected` 或 `Can't verify CSRF token authenticity Completed 422 Unprocessable` 的错误。

这可以通过覆盖默认 headers 来实现，例如在 `/etc/gitlab/gitlab.rb` 中指定：

```ruby
 nginx['proxy_set_headers'] = {
  "X-Forwarded-Proto" => "http",
  "CUSTOM_HEADER" => "VALUE"
 }
```
保存文件并[重新配置GitLab](https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure) 使更改生效。

通过这种方法，您可以按需指定 NGINX 支持的任何 header。

## 配置 `trusted_proxies` 以及 NGINX `real_ip` 模块

默认情况下，NGINX 和极狐GitLab 记录所连接客户端的 IP 地址。

如果您的极狐GitLab 实例位于反向代理之后，您可能不需要代理 IP 地址显示为客户地址。

您可以通过添加反向代理到 `real_ip_trusted_addresses` 列表，使 NGINX 查找一个不同的地址去使用。

```ruby
# Each address is added to the the NGINX config as 'set_real_ip_from <address>;'
nginx['real_ip_trusted_addresses'] = [ '192.168.1.0/24', '192.168.2.1', '2001:0db8::/32' ]
# other real_ip config options
nginx['real_ip_header'] = 'X-Forwarded-For'
nginx['real_ip_recursive'] = 'on'
```

选项说明:

- <http://nginx.org/en/docs/http/ngx_http_realip_module.html>

默认情况下，Omnibus GitLab 使用 `real_ip_trusted_addresses` 中的 IP 地址作为 GitLab 信任代理，用户不会使用那些列表中的 IP 进行登录。

保存文件并[重新配置GitLab](https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure) 使更改生效。

## 配置 HTTP2 协议

<!--By default, when you specify that your GitLab instance should be reachable
through HTTPS by specifying `external_url "https://gitlab.example.com"`,
[http2 protocol](https://tools.ietf.org/html/rfc7540) is also enabled.

The Omnibus GitLab package sets required ssl_ciphers that are compatible with
http2 protocol.

If you are specifying custom ssl_ciphers in your configuration and a cipher is
in [http2 cipher blacklist](https://tools.ietf.org/html/rfc7540#appendix-A), once you try to reach your GitLab instance you will
be presented with `INADEQUATE_SECURITY` error in your browser.

Consider removing the offending ciphers from the cipher list. Changing ciphers
is only necessary if you have a very specific custom setup.

For more information on why you would want to have http2 protocol enabled, check out
the [http2 whitepaper](https://assets.wp.nginx.com/wp-content/uploads/2015/09/NGINX_HTTP2_White_Paper_v4.pdf?_ga=1.127086286.212780517.1454411744).

If changing the ciphers is not an option you can disable http2 support by
specifying in `/etc/gitlab/gitlab.rb`:-->

当您指定了 `external_url "https://gitlab.example.com"`，使您的极狐GitLab 实例可以通过 HTTPS 访问时，默认情况下，[http2 协议](https://tools.ietf.org/html/rfc7540)也同时开启。

如果您在配置中指定了自定义的 ssl 密码，密码在 [http2 密码黑名单](https://tools.ietf.org/html/rfc7540#appendix-A)中，一旦您尝试访问您的极狐GitLab 实例，在浏览器中显示 `INADEQUATE_SECURITY` 错误。

考虑从密码列表中移除有问题的密码。仅当您具有非常特定的自定义设置，且必要时更改密码。

关于为何您想要启用 http2 协议的更多信息，查看 [http2 白皮书](https://assets.wp.nginx.com/wp-content/uploads/2015/09/NGINX_HTTP2_White_Paper_v4.pdf?_ga=1.127086286.212780517.1454411744)。

如果您无法选择更改密码，您也可以在 `/etc/gitlab/gitlab.rb` 中，指定禁用 http2 支持。

```ruby
nginx['http2_enabled'] = false
```

保存文件并[重新配置GitLab](https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure) 使更改生效。

<!--NOTE:
The `http2` setting only works for the main GitLab application and not for the other services.-->

NOTE:
`http2` 设置仅适用于主 GitLab 应用程序，不适用于其他服务。

## 使用非捆绑的网络服务器
<!--
By default, Omnibus GitLab installs GitLab with bundled NGINX.
Omnibus GitLab allows webserver access through the `gitlab-www` user, which resides
in the group with the same name. To allow an external webserver access to
GitLab, the external webserver user needs to be added to the `gitlab-www` group.

To use another web server like Apache or an existing NGINX installation you
will have to perform the following steps:

1. **Disable bundled NGINX**

   In `/etc/gitlab/gitlab.rb` set:

   ```ruby
   nginx['enable'] = false
   ```

1. **Set the username of the non-bundled web-server user**

   By default, Omnibus GitLab has no default setting for the external webserver
   user, you have to specify it in the configuration. For Debian/Ubuntu the
   default user is `www-data` for both Apache/NGINX whereas for RHEL/CentOS
   the NGINX user is `nginx`.

   Make sure you have first installed Apache/NGINX so the webserver user is created, otherwise omnibus will fail while reconfiguring.

   Let's say for example that the webserver user is `www-data`.
   In `/etc/gitlab/gitlab.rb` set:

   ```ruby
   web_server['external_users'] = ['www-data']
   ```

   This setting is an array so you can specify more than one user to be added to `gitlab-www` group.

   Run `sudo gitlab-ctl reconfigure` for the change to take effect.

   If you are using SELinux and your web server runs under a restricted SELinux profile you may have to [loosen the restrictions on your web server](https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server/apache#selinux-modifications).

   Make sure that the webserver user has the correct permissions on all directories used by external web-server, otherwise you will receive `failed (XX: Permission denied) while reading upstream` errors.

1. **Add the non-bundled web-server to the list of trusted proxies**

   Normally, Omnibus GitLab defaults the list of trusted proxies to what was
   configured in the `real_ip` module for the bundled NGINX.

   For non-bundled web-servers the list needs to be configured directly, and should
   include the IP address of your web-server if it is not on the same machine as GitLab.
   Otherwise, users will be shown as being signed in from your web-server's IP address.

   ```ruby
   gitlab_rails['trusted_proxies'] = [ '192.168.1.0/24', '192.168.2.1', '2001:0db8::/32' ]
   ```

1. **(Optional) Set the right GitLab Workhorse settings if using Apache**

   Apache cannot connect to a UNIX socket but instead needs to connect to a
   TCP Port. To allow GitLab Workhorse to listen on TCP (by default port 8181)
   edit `/etc/gitlab/gitlab.rb`:

   ```ruby
   gitlab_workhorse['listen_network'] = "tcp"
   gitlab_workhorse['listen_addr'] = "127.0.0.1:8181"
   ```

   Run `sudo gitlab-ctl reconfigure` for the change to take effect.

1. **Download the right web server configs**

   Go to [GitLab recipes repository](https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server) and look for the omnibus
   configs in the webserver directory of your choice. Make sure you pick the
   right configuration file depending whether you choose to serve GitLab with
   SSL or not. The only thing you need to change is `YOUR_SERVER_FQDN` with
   your own FQDN and if you use SSL, the location where your SSL keys currently
   reside. You also might need to change the location of your log files.
-->

默认情况下，Omnibus GitLab 安装捆绑有 NGINX 的极狐GitLab 实例。Omnibus GitLab 允许 `gitlab-www` 用户访问网络服务器，`gitlab-www` 用户位于同名的群组中。要允许外部网络服务器访问极狐GitLab 实例，需要将外部网络服务器用户添加到 `gitlab-www` 群组。

要使用其它 Web 服务器，例如 Apache 或已有的 NGINX 实例，您需要执行以下步骤：

1. **禁用捆绑的 NGINX**

	在 `/etc/gitlab/gitlab.rb` 中设置：
	
	  ```ruby
	  nginx['enable'] = false
	  ```
2. **设置非捆绑网络服务器用户的用户名**

	默认情况下，Omnibus GitLab 没有针对外部网络服务器用户的默认设置，您必须在配置中进行指定。对于 Debian/Ubuntu 操作系统，Apache/NGINX 的默认用户是 `www-data`，而对于 RHEL/CentOS 操作系统，NGINX 用户是 `nginx`。
	
	确保您提前安装了 Apache/NGINX，这样已创建了网络服务器用户，否则重新配置时 Omnibus 会失败。
	
	例如网络服务器用户是 `www-data`。在 `/etc/gitlab/gitlab.rb` 中设置：
	
	   ```ruby
	   web_server['external_users'] = ['www-data']
	   ```	

	该设置是一个数组，您可以指定添加多个用户到 `gitlab-www` 群组。

	执行 `sudo gitlab-ctl reconfigure` 命令以使更改生效。
	
	如果您使用 SELinux，并且您的 Web 服务器在 SELinux 限制文件下运行，您可能需要[放宽对 Web 服务器的限制](https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web -server/apache#selinux-modifications）。
	
	确保网络服务器用户对外部网络服务器使用的所有目录具有正确的权限，否则您将收到 `failed (XX: Permission denied) while reading upstream` 错误。
	
3. **将非捆绑的网络服务器添加到受信任代理列表中**

	通常 Omnibus GitLab 的可信列表默认为捆绑的 NGINX 配置中，`real_ip` 模块的配置内容。
	
	对于非捆绑的网络服务器，需要直接配置该列表，如果网络服务器与极狐GitLab 实例不在同一台机器上，列表中需要包括您的网络服务器 IP 地址。否则，用户将显示为从您的网络服务器的 IP 地址登录。
	
	   ```ruby
	   gitlab_rails['trusted_proxies'] = [ '192.168.1.0/24', '192.168.2.1', '2001:0db8::/32' ]
	   ```	

4. **（可选）使用 Apache 时，设置正确的 GitLab Workhorse**

	Apache 需要连接到 TCP 端口而不是 UNIX 套接字。要允许 GitLab Workhorse 监听 TCP 端口（默认为 8181），编辑 `/etc/gitlab/gitlab.rb`：
	
	   ```ruby
	   gitlab_workhorse['listen_network'] = "tcp"
	   gitlab_workhorse['listen_addr'] = "127.0.0.1:8181"
	   ```
	   
	执行 `sudo gitlab-ctl reconfigure` 命令以使更改生效。
	
5. **下载正确的网络服务器配置**

	访问 [GitLab recipes repository](https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server)，在 webserver 目录中，查找 Omnibus 配置。根据您是否使用 SSL，挑选正确的配置文件。您唯一需要修改的是 `YOUR_SERVER_FQDN`，修改为您自己的 FQDN，如果您使用 SSL，则修改为您的 SSL 密钥当前所在的位置。您可能还需要更改日志文件的位置。	

## 设置 NGINX 监听地址
<!--
By default NGINX will accept incoming connections on all local IPv4 addresses.
You can change the list of addresses in `/etc/gitlab/gitlab.rb`.
-->

默认情况下，NGINX 将接受所有本地 IPv4 地址上的传入连接。您可以更改 `/etc/gitlab/gitlab.rb` 上的地址列表。

```ruby
 # Listen on all IPv4 and IPv6 addresses
nginx['listen_addresses'] = ["0.0.0.0", "[::]"]
registry_nginx['listen_addresses'] = ['*', '[::]']
mattermost_nginx['listen_addresses'] = ['*', '[::]']
pages_nginx['listen_addresses'] = ['*', '[::]']
```

## 设置 NGINX 监听端口
<!--
By default NGINX will listen on the port specified in `external_url` or
implicitly use the right port (80 for HTTP, 443 for HTTPS). If you are running
GitLab behind a reverse proxy, you may want to override the listen port to
something else. For example, to use port 8081:
-->

默认情况下，NGINX 将监听 `external_url` 中的指定端口，或使用正确的端口（HTTP 为 80，HTTPS 为 443）。如果您在反向代理后运行极狐GitLab，您可能想要使用其它的监听端口。以使用 8081 为例：

```ruby
nginx['listen_port'] = 8081
```

## 外部代理和负载均衡器 SSL 终止
<!--
By default, Omnibus GitLab auto-detects whether to use SSL if `external_url`
contains `https://` and configures NGINX for SSL termination. 
However, if configuring GitLab to run behind a reverse proxy or an external load balancer,
some environments may want to terminate SSL outside the GitLab application. To do this,
edit `/etc/gitlab/gitlab.rb` to prevent the bundled NGINX from handling SSL termination:
-->

如果 `external_url` 包含 `https://`，默认情况下，Omnibus GitLab 将自动检测是否使用 SSL，并为 NGINX 配置 SSL 终止。

然而，如果将极狐GitLab 实例配置为在反向代理或外部负载均衡器之后运行，在某些环境可能希望在 GitLab 应用程序之外终止 SSL。此时，编辑 `/etc/gitlab/gitlab.rb` 以组织绑定的 NGINX 处理 SSL 终止：

```ruby
nginx['listen_port'] = 80
nginx['listen_https'] = false
```
<!--
Other bundled components (Registry, Pages, etc) use a similar strategy for
proxied SSL. Set the particular component's `*_external_url` with `https://` and
prefix the `nginx[...]` configuration with the component name. For example, for
Registry use the following configuration:
-->

其它捆绑组件（Registry、Pages 等）使用类似的代理 SSL 策略。设置特定组件的 `*_external_url` 时，使用 `https://`，并使用组件名称作为 `nginx[...]` 配置的前缀。例如，对于 Registry 使用如下配置：

```ruby
registry_external_url 'https://registry.example.com'

registry_nginx['listen_port'] = 80
registry_nginx['listen_https'] = false
```
<!--
The same format can be used for Pages (`pages_` prefix) and Mattermost (`mattermost_` prefix).

Note that you may need to configure your reverse proxy or load balancer to
forward certain headers (e.g. `Host`, `X-Forwarded-Ssl`, `X-Forwarded-For`,
`X-Forwarded-Port`) to GitLab (and Mattermost if you use one). You may see improper redirections or errors
(e.g. "422 Unprocessable Entity", "Can't verify CSRF token authenticity") if
you forget this step. For more information, see:

- <https://stackoverflow.com/questions/16042647/whats-the-de-facto-standard-for-a-reverse-proxy-to-tell-the-backend-ssl-is-used>
- <https://websiteforstudents.com/setup-apache2-reverse-proxy-nginx-ubuntu-17-04-17-10/>
Some cloud provider services, such as AWS Certificate Manager (ACM), do not allow the download of certificates. This prevents them from being used to terminate on the GitLab instance. If SSL is desired between such a cloud service and the GitLab instance, another certificate must be used on the GitLab instance.
-->

相同格式可用于 Pages （`pages_` 前缀）和 Mattermost（`mattermost_` 前缀）。

注意您可能需要配置反向代理或负载均衡器以转发特定的 headers（例如 `Host`, `X-Forwarded-Ssl`, `X-Forwarded-For`, `X-Forwarded-Port`）到极狐GitLab 实例（使用 Mattermost 时亦然）。如果您忘记了这一步，您可能会看到不正确的重定向或错误（例如"422 Unprocessable Entity", "Can't verify CSRF token authenticity"）。有关更多信息，请参阅：

* <https://stackoverflow.com/questions/16042647/whats-the-de-facto-standard-for-a-reverse-proxy-to-tell-the-backend-ssl-is-used>
* <https://websiteforstudents.com/setup-apache2-reverse-proxy-nginx-ubuntu-17-04-17-10/> 一些云提供商的服务，例如 AWS Certificate Manager (ACM)，不允许下载证书。这可以防止它们被用于终止 GitLab 实例。如果需要在此类云服务和 GitLab 实例之间使用 SSL，则必须在 GitLab 实例上使用另一个证书。

## 设置 HTTP Strict Transport Security（HSTS）
<!--
By default GitLab enables Strict Transport Security which informs browsers that
they should only contact the website using HTTPS. When a browser visits a
GitLab instance even once, it will remember to no longer attempt insecure connections,
even when the user is explicitly entering a `http://` URL. Such a URL will be automatically redirected by the browser to `https://` variant.
-->

默认情况下，极狐GitLab 启用 HSTS，使得浏览器只使用 HTTPS 与网站通信。当浏览器访问实例一次，会记住不再尝试不安全的连接，即使用户明确输入了 `http://` 的 URL。这样的 URL 会被浏览器自动重定向到 `https://`。

```ruby
nginx['hsts_max_age'] = 31536000
nginx['hsts_include_subdomains'] = false
```
<!--
By default `max_age` is set for one year, this is how long browser will remember to only connect through HTTPS.
Setting `max_age` to 0 will disable this feature. For more information see:
-->

默认情况下 `max_age` 设置为 1 年，这是浏览器记住只通过 HTTPS 进行连接的时长。`max_age`  设置为 0 将禁用该特性，更多信息请参阅：

- <https://www.nginx.com/blog/http-strict-transport-security-hsts-and-nginx/>
<!--
NOTE:
The HSTS settings only work for the main GitLab application and not for the other services.
-->

NOTE:
HSTS 设置仅适用于极狐GitLab 主应用，不适用于其它服务。

## 设置 Referrer-Policy header
<!--
By default, GitLab sets the `Referrer-Policy` header to `strict-origin-when-cross-origin` on all responses.

This makes the client send the full URL as referrer when making a same-origin request but only send the
origin when making cross-origin requests.

To set this header to a different value:
-->

默认情况下，极狐GitLab 所有响应的 Referrer-Policy header 设置为 `strict-origin-when-cross-origin`。

这使得客户端在发出同源请求时，发送完整的 URL 作为 referrer，但仅在发出跨域请求时发送源。

将该表头设置为不同的值：

```ruby
nginx['referrer_policy'] = 'same-origin'
```
<!--
You can also disable this header to make the client use its default setting:
-->

您也可以禁用该 header，使客户端使用默认设置：

```ruby
nginx['referrer_policy'] = false
```
<!--
Note that setting this to `origin` or `no-referrer` would break some features in GitLab that require the full referrer URL.
-->

注意，将其设置为 `origin` 或 `no-referrer`，会破坏极狐GitLab 中需要完整引用 URL 的某些功能。

- <https://www.w3.org/TR/referrer-policy/>

## 禁用 Gzip 压缩
<!--
By default, GitLab enables Gzip compression for text data over 10240 bytes. To
disable this behavior:
-->

默认情况下，极狐GitLab 对超过 10240 字节的文本数据启用 Gzip 压缩。要禁用此行为：

```ruby
nginx['gzip_enabled'] = false
```
<!--
NOTE:
The `gzip` setting only works for the main GitLab application and not for the other services.
-->

NOTE:
`gzip` 设置仅适用于极狐GitLab 主应用，不适用于其它服务。

## 使用自定义 SSL 密码
<!--
By default GitLab is using SSL ciphers that are combination of testing on <https://gitlab.com> and various best practices contributed by the GitLab community.

However, you can change the ssl ciphers by adding to `gitlab.rb`:
-->

默认情况下您可以使用 SSL 密码，您可以添加以下配置到 `gitlab.rb` 来更改 SSL 密码：

```ruby
  nginx['ssl_ciphers'] = "CIPHER:CIPHER1"
```

<!--
and running reconfigure.

You can also enable `ssl_dhparam` directive.

First, generate `dhparams.pem` with `openssl dhparam -out dhparams.pem 2048`. Then, in `gitlab.rb` add a path to the generated file, for example:
-->

运行重新配置命令。

您还可以启用 `ssl_dhparam` 指令。

首先，使用 `openssl dhparam -out dhparams.pem 2048` 生成 `dhparams.pem`。然后，在 `gitlab.rb` 中添加生成文件的路径，例如：

```ruby
  nginx['ssl_dhparam'] = "/etc/gitlab/ssl/dhparams.pem"
```
<!--
After the change run `sudo gitlab-ctl reconfigure`.
-->

更改之后，执行 `sudo gitlab-ctl reconfigure` 命令。

## 启用两步 SSL 客户端身份验证
<!--
To require web clients to authenticate with a trusted certificate, you can enable 2-way SSL by adding to `gitlab.rb`:
-->

要求 Web 客户端使用受信任的证书进行身份验证，您可以通过添加以下内容到 `gitlab.rb` 来启用两步 SSL 验证：

```ruby
  nginx['ssl_verify_client'] = "on"
```
<!--
and running reconfigure.

These additional options NGINX supports for configuring SSL client authentication can also be configured:
-->

运行重新配置命令。

作为 NGINX 支持的额外选项，SSL 客户端认证还可以如下配置：

```ruby
  nginx['ssl_client_certificate'] = "/etc/pki/tls/certs/root-certs.pem"
  nginx['ssl_verify_depth'] = "2"
```
<!--
After making the changes run `sudo gitlab-ctl reconfigure`.
-->

更改之后，执行 `sudo gitlab-ctl reconfigure` 命令。

## 配置 `robots.txt`
<!--
To configure [`robots.txt`](https://www.robotstxt.org/robotstxt.html) for your instance, specify a custom `robots.txt` file by adding a [custom NGINX configuration](#inserting-custom-nginx-settings-into-the-gitlab-server-block):
-->

要为您的实例配置 [`robots.txt`](https://www.robotstxt.org/robotstxt.html)，指定一个自定义的 `robots.txt` 文件，并添加自定义 NGINX 配置。

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   nginx['custom_gitlab_server_config'] = "rewrite ^/robots.txt /var/opt/gitlab/robots.txt last;"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 在 server block 中添加自定义 NGINX 设置
<!--
Please keep in mind that these custom settings may create conflicts if the
same settings are defined in your `gitlab.rb` file.

If you need to add custom settings into the NGINX `server` block for GitLab for
some reason you can use the following setting.
-->

请注意，如果在您的 `gitlab.rb` 文件中定义了相同的设置，这些自定义设置可能会发生冲突。

如果出于某种原因，您需要将自定义设置添加到 NGINX server block 中，您可以使用以下设置。

```ruby
# Example: block raw file downloads from a specific repository
nginx['custom_gitlab_server_config'] = "location ^~ /foo-namespace/bar-project/raw/ {\n deny all;\n}\n"
```
<!--
Run `gitlab-ctl reconfigure` to rewrite the NGINX configuration and restart
NGINX.

This inserts the defined string into the end of the `server` block of
`/var/opt/gitlab/nginx/conf/gitlab-http.conf`.
-->

执行 `gitlab-ctl reconfigure` 命令，重写 NGINX 配置并重启 NGINX。

定义的字符串将被插入到 `/var/opt/gitlab/nginx/conf/gitlab-http.conf` 中的 `server` block 末尾。

### 注意

- 如果您在添加新的 location，需要在字符串中包含以下内容，或已包含在 NGINX 配置中。

  ```conf
  proxy_cache off;
  proxy_pass http://gitlab-workhorse;
  ```

  没有以上配置，任何 sub-location 将返回 404。查看 [GitLab CE Issue #30619](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/30619)。

- 不能添加根路径 `/` location 或 `/assets` location，因为已经存在于 `gitlab-http.conf`。

## 在 NGINX 配置中插入自定义设置
<!--
If you need to add custom settings into the NGINX configuration, for example to include
existing server blocks, you can use the following setting.
-->

如果您需要在 NGINX 配置中插入自定义设置，例如包含已存在的 server blocks，您可以使用如下设置。

```ruby
# Example: include a directory to scan for additional config files
nginx['custom_nginx_config'] = "include /etc/nginx/conf.d/*.conf;"
```
<!--
Run `gitlab-ctl reconfigure` to rewrite the NGINX configuration and restart
NGINX.

This inserts the defined string into the end of the `http` block of
`/var/opt/gitlab/nginx/conf/nginx.conf`.
-->

执行 `gitlab-ctl reconfigure`，重写 NGINX 配置并重启 NGINX。

定义的字符串将被插入到 `/var/opt/gitlab/nginx/conf/nginx.conf` 中的 `http` block 末尾。

## 自定义错误页面
<!--
You can use `custom_error_pages` to modify text on the default GitLab error page.
This can be used for any valid HTTP error code; e.g 404, 502.

As an example the following would modify the default 404 error page.
-->

您可以使用 `custom_error_pages` 修改默认的极狐GitLab 错误页面的文案。适用于任何有效的 HTTP 错误码；例如 404，502。

参考以下示例修改默认的 404 错误页面。

```ruby
nginx['custom_error_pages'] = {
  '404' => {
    'title' => 'Example title',
    'header' => 'Example header',
    'message' => 'Example message'
  }
}
```

将生成下方的 404 页面。

![custom 404 error page](img/error_page_example.png)

执行 `gitlab-ctl reconfigure`，重写 NGINX 配置并重启 NGINX。

## 使用已有的 Passenger/NGINX 实例
<!--
In some cases you may want to host GitLab using an existing Passenger/NGINX
installation but still have the convenience of updating and installing using
the omnibus packages.

NOTE:
When disabling NGINX, you won't be able to access
other services included by Omnibus, like Grafana, Mattermost, etc. unless
you manually add them in `nginx.conf`.
-->

在某些情况下，您可能想要极狐GItLab 实例使用已有的 Passenger/NGINX 实例，但同时保留使用 Omnibus 安装包进行升级和安装时的便利性。

NOTE:
当禁用 NGINX 时，您将无法访问 Omnibus 的其它服务，例如 Grafana，Mattermost 等等，除非您在 `nginx.conf` 中手动添加。

### 配置
<!--
First, you'll need to setup your `/etc/gitlab/gitlab.rb` to disable the built-in
NGINX and Puma:
-->

首先，您需要设置 `/etc/gitlab/gitlab.rb` 以禁用内建的 NGINX 和 Puma。

```ruby
# Define the external url
external_url 'http://git.example.com'

# Disable the built-in nginx
nginx['enable'] = false

# Disable the built-in puma
puma['enable'] = false

# Set the internal API URL
gitlab_rails['internal_api_url'] = 'http://git.example.com'

# Define the web server process user (ubuntu/nginx)
web_server['external_users'] = ['www-data']
```
<!--
Make sure you run `sudo gitlab-ctl reconfigure` for the changes to take effect.

NOTE:
If you are running a version older than 8.16.0, you will have to
manually remove the Unicorn service file (`/opt/gitlab/service/unicorn`), if
exists, for reconfigure to succeed.
-->

确保执行 `sudo gitlab-ctl reconfigure` 命令使更改生效。

### Vhost (server block)
<!--
NOTE:
GitLab 13.5 changed the default workhorse socket location from `/var/opt/gitlab/gitlab-workhorse/socket` to `/var/opt/gitlab/gitlab-workhorse/sockets/socket`. Please update the following configuration accordingly if upgrading from versions older than 13.5.

Then, in your custom Passenger/NGINX installation, create the following site
configuration file:
-->

然后，在您的自定义 Passenger/NGINX 实例中，创建以下站点配置文件：

```plaintext
upstream gitlab-workhorse {
  server unix://var/opt/gitlab/gitlab-workhorse/sockets/socket fail_timeout=0;
}

server {
  listen *:80;
  server_name git.example.com;
  server_tokens off;
  root /opt/gitlab/embedded/service/gitlab-rails/public;

  client_max_body_size 250m;

  access_log  /var/log/gitlab/nginx/gitlab_access.log;
  error_log   /var/log/gitlab/nginx/gitlab_error.log;

  # Ensure Passenger uses the bundled Ruby version
  passenger_ruby /opt/gitlab/embedded/bin/ruby;

  # Correct the $PATH variable to included packaged executables
  passenger_env_var PATH "/opt/gitlab/bin:/opt/gitlab/embedded/bin:/usr/local/bin:/usr/bin:/bin";

  # Make sure Passenger runs as the correct user and group to
  # prevent permission issues
  passenger_user git;
  passenger_group git;

  # Enable Passenger & keep at least one instance running at all times
  passenger_enabled on;
  passenger_min_instances 1;

  location ~ ^/[\w\.-]+/[\w\.-]+/(info/refs|git-upload-pack|git-receive-pack)$ {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  location ~ ^/[\w\.-]+/[\w\.-]+/repository/archive {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  location ~ ^/api/v3/projects/.*/repository/archive {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ ^/[\w\.-]+/[\w\.-]+/builds/download {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ /ci/api/v1/builds/[0-9]+/artifacts {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ /api/v4/jobs/[0-9]+/artifacts {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }


  # For protocol upgrades from HTTP/1.0 to HTTP/1.1 we need to provide Host header if its missing
  if ($http_host = "") {
  # use one of values defined in server_name
    set $http_host_with_default "git.example.com";
  }

  if ($http_host != "") {
    set $http_host_with_default $http_host;
  }

  location @gitlab-workhorse {

    ## https://github.com/gitlabhq/gitlabhq/issues/694
    ## Some requests take more than 30 seconds.
    proxy_read_timeout      3600;
    proxy_connect_timeout   300;
    proxy_redirect          off;

    # Do not buffer Git HTTP responses
    proxy_buffering off;

    proxy_set_header    Host                $http_host_with_default;
    proxy_set_header    X-Real-IP           $remote_addr;
    proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto   $scheme;

    proxy_pass http://gitlab-workhorse;

    ## The following settings only work with NGINX 1.7.11 or newer
    #
    ## Pass chunked request bodies to gitlab-workhorse as-is
    # proxy_request_buffering off;
    # proxy_http_version 1.1;
  }

  ## Enable gzip compression as per rails guide:
  ## http://guides.rubyonrails.org/asset_pipeline.html#gzip-compression
  ## WARNING: If you are using relative urls remove the block below
  ## See config/application.rb under "Relative url support" for the list of
  ## other files that need to be changed for relative url support
  location ~ ^/(assets)/ {
    root /opt/gitlab/embedded/service/gitlab-rails/public;
    gzip_static on; # to serve pre-gzipped version
    expires max;
    add_header Cache-Control public;
  }

  ## To access Grafana
  location /-/grafana/ {
    proxy_pass http://localhost:3000/;
  }

  error_page 502 /502.html;
}
```
<!--
Don't forget to update `git.example.com` in the above example to be your server URL.

If you wind up with a 403 forbidden, it's possible that you haven't enabled passenger in `/etc/nginx/nginx.conf`,
to do so simply uncomment:

```plaintext
# include /etc/nginx/passenger.conf;
```

Then run `sudo service nginx reload`.
-->

不要忘记将上面示例中的 `git.example.com` 更新为您的服务器 URL。

如果您收到 403 forbidden 返回，可能是您没有在 `/etc/nginx/nginx.conf` 中启用 passenger，只需取消注释即可：

```plaintext
# include /etc/nginx/passenger.conf;
```

然后执行 `sudo service nginx reload` 命令。

## 启用/禁用 nginx_status
<!--
By default you will have an NGINX health-check endpoint configured at `127.0.0.1:8060/nginx_status` to monitor your NGINX server status.
-->

默认情况下，NGINX 健康检查端点配置在 `127.0.0.1:8060/nginx_status`，以监控您的 NGINX 服务器状态。

### 展示以下信息

```plaintext
Active connections: 1
server accepts handled requests
 18 18 36
Reading: 0 Writing: 1 Waiting: 0
```
<!--
- Active connections – Open connections in total.
- 3 figures are shown.
  - All accepted connections.
  - All handled connections.
  - Total number of handled requests.
- Reading: NGINX reads request headers
- Writing: NGINX reads request bodies, processes requests, or writes responses to a client
- Waiting: Keep-alive connections. This number depends on the keepalive-timeout.
-->

* Active connections - 开放的连接总数。
* 展示 3 个指标数字。
	* 所有接受的连接。
	* 所有处理的连接。
	* 处理的请求总数。
* Reading：NGINX 读取的请求 header。
* Writing：NGINX 读取请求体，处理请求，或将响应写入客户端。
* Waiting：Keep-alive 连接。该数字取决于 keepalive-timeout。

### 配置项

编辑 `/etc/gitlab/gitlab.rb`：

```ruby
nginx['status'] = {
  "listen_addresses" => ["127.0.0.1"],
  "fqdn" => "dev.example.com",
  "port" => 9999,
  "options" => {
    "access_log" => "on", # Disable logs for stats
    "allow" => "127.0.0.1", # Only allow access from localhost
    "deny" => "all" # Deny access to anyone else
  }
}
```

如果您发现该服务不适用于您当前的基础设施，您可以通过以下命令禁用：

```ruby
nginx['status'] = {
  'enable' => false
}
```

确保执行 `sudo gitlab-ctl reconfigure` 命令使更改生效。

#### 警告
<!--
To ensure that user uploads are accessible your NGINX user (usually `www-data`)
should be added to the `gitlab-www` group. This can be done using the following command:
-->

为了确保可以访问用户上传的内容，您的 NGINX 用户（通常为 `www-data`）应被添加到 `gitlab-www` 群组中。使用以下命令：

```shell
sudo usermod -aG gitlab-www www-data
```

## 模板
<!--
Other than the Passenger configuration in place of Unicorn and the lack of HTTPS
(although this could be enabled) these files are mostly identical to:

- [Bundled GitLab NGINX configuration](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/files/gitlab-cookbooks/gitlab/templates/default/nginx-gitlab-http.conf.erb)

Don't forget to restart NGINX to load the new configuration (on Debian-based
systems `sudo service nginx restart`).
-->

处理替代 Unicorn 的 Passenger 配置和缺少 HTTPS（尽管可以启用）外，这些文件大部分与以下内容相同：

- [Bundled GitLab NGINX configuration](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/files/gitlab-cookbooks/gitlab/templates/default/nginx-gitlab-http.conf.erb)

不要忘记重启 NGINX，加载新的配置（在 Debian 系统中为 `sudo service nginx restart`）。

## 故障排查

### 400 Bad Request: too many Host headers
<!--
Make sure you don't have the `proxy_set_header` configuration in
`nginx['custom_gitlab_server_config']` settings and instead use the
['proxy_set_headers'](#external-proxy-and-load-balancer-ssl-termination) configuration in your `gitlab.rb` file.
-->
确保在 `nginx['custom_gitlab_server_config']` 设置中没有 `proxy_set_header` 配置，而是在 `gitlab.rb` 文件中使用 ['proxy_set_headers'](#外部代理和负载均衡器 SSL 终止) 配置。

### `javax.net.ssl.SSLHandshakeException: Received fatal alert: handshake_failure`
<!--
Starting with GitLab 10, the Omnibus GitLab package no longer supports TLSv1 protocol by default.
This can cause connection issues with some older Java based IDE clients when interacting with
your GitLab instance.
We strongly urge you to upgrade ciphers on your server, similar to what was mentioned
in [this user comment](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/624#note_299061).

If it is not possible to make this server change, you can default back to the old
behavior by changing the values in your `/etc/gitlab/gitlab.rb`:
-->

从 GitLab 10 开始，Omnibus GitLab 包默认不再支持 TLSv1 协议。在与 GitLab 实例交互时，这可能会导致某些基于 Java 的旧 IDE 客户端出现连接问题。我们强烈建议您升级服务器上的密码，类似于[此用户评论](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/624#note_299061)中的内容。

如果无法更改此服务器，则可以通过更改 `/etc/gitlab/gitlab.rb` 中的值来默认返回旧协议：

```ruby
nginx['ssl_protocols'] = "TLSv1 TLSv1.1 TLSv1.2 TLSv1.3"
```

### 私钥和证书不匹配
<!--
If you see `x509 certificate routines:X509_check_private_key:key values mismatch)` in the NGINX logs (`/var/log/gitlab/nginx/current` by default for Omnibus), there is a mismatch between your private key and certificate.

To fix this, you will need to match the correct private key with your certificate.

To ensure you have the correct key and certificate, you can ensure that the modulus of the private key and certificate match:
-->

如果在 NGINX 日志（Omnibus 默认为 `/var/log/gitlab/nginx/current`）中看到 `x509 certificate routines:X509_check_private_key:key 
 values mismatch)`，您的私钥和证书不匹配。
 
 要解决该问题，您需要将正确的私钥与您的证书相匹配。
 
 为确保您拥有正确的密钥和证书，您可以确保私钥和证书的模数匹配：

```shell
/opt/gitlab/embedded/bin/openssl rsa -in /etc/gitlab/ssl/gitlab.example.com.key -noout -modulus | openssl sha1

/opt/gitlab/embedded/bin/openssl x509 -in /etc/gitlab/ssl/gitlab.example.com.crt -noout -modulus| openssl sha1
```

一旦您确认它们匹配，您需要重新配置并重新加载 NGINX：

```shell
sudo gitlab-ctl reconfigure
sudo gitlab-ctl hup nginx
```

### Request Entity Too Large
<!--
If you see `Request Entity Too Large` in the [NGINX logs](https://docs.gitlab.com/ee/administration/logs.html#nginx-logs),
you will need to increase the [Client Max Body Size](http://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size).
You may encounter this error if you have increased the [Max import size](https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html#max-import-size).
In a Kubernetes-based GitLab installation, this setting is
[named differently](https://docs.gitlab.com/charts/charts/gitlab/webservice/#proxybodysize).

To increase the `client_max_body_size`, you will need to set the value in your `/etc/gitlab/gitlab.rb`:
-->

如果您在 [NGINX 日志](https://docs.gitlab.com/ee/administration/logs.html#nginx-logs) 中看到 `Request Entity Too Large`，您需要增加 [Client Max Body Size](http://nginx.org/en/docs/http/ngx_http_core_module.html#client_ max_body_size)。如果您增加了 [Max import size](https://docs.gitlab.cn/jhe/user/admin_area/settings/account_and_limit_settings.html#max-import-size)，可能会遇到此错误。在基于 Kubernetes 的安装中，该设置的[名称不同](https://docs.gitlab.com/charts/charts/gitlab/webservice/#proxybodysize)。

要增加 `client_max_body_size`，您需要在 `/etc/gitlab/gitlab.rb` 中设置该值：

```ruby
nginx['client_max_body_size'] = '250m'
```

确保执行 `sudo gitlab-ctl reconfigure` 和 `sudo gitlab-ctl hup nginx` 命令，使 NGINX [加载更新配置](http://nginx.org/en/docs/control.html)。要增加 `client_max_body_size`：
<!--
Make sure you run `sudo gitlab-ctl reconfigure` and run `sudo gitlab-ctl hup nginx` to cause NGINX to
[reload the with the updated configuration](http://nginx.org/en/docs/control.html)
To increase the `client_max_body_size`:
-->
1. 编辑 `/etc/gitlab/gitlab.rb` 并设置更优值：

   ```ruby
   nginx['client_max_body_size'] = '250m'
   ```

1. 重新配置极狐Gitlab 和 [HUP](https://nginx.org/en/docs/control.html) NGINX，平滑加载更新的配置：

   ```shell
   sudo gitlab-ctl reconfigure
   sudo gitlab-ctl hup nginx
   ```
