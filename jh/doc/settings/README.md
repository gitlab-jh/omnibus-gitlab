---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 配置 Omnibus GitLab

- [NGINX](nginx.md)
- [SMTP](smtp.md)
- [SSL](ssl.md)

