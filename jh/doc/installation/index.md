---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
comments: false
---

# 使用 Omnibus 安装包安装极狐GitLab

## 前提条件

<!--
- [Installation Requirements](https://docs.gitlab.com/ee/install/requirements.html).
- If you want to access your GitLab instance via a domain name, like `mygitlabinstance.com`,
  make sure the domain correctly points to the IP of the server where GitLab is being
  installed. You can check this using the command `host mygitlabinstance.com`.
- If you want to use HTTPS on your GitLab instance, make sure you have the SSL
  certificates for the domain ready. (Note that certain components like
  Container Registry which can have their own subdomains requires certificates for
  those subdomains also.)
- If you want to send notification emails, install and configure a mail server (MTA)
  like sendmail. Alternatively, you can use other [third party SMTP servers](../settings/smtp.md).
-->

* [安装需求](https://docs.gitlab.cn/ee/install/requirements.html)。
* 如果您想要通过域名访问您的极狐GitLab 实例，例如`mygitlabinstance.com`，确保域名正确地指向安装了极狐GitLab 的服务器的 IP。您可以使用 `host mygitlabinstance.com` 命令进行检查。
* 如果您想要在实例上使用 HTTPS，请确保已准备好域的 SSL 证书。（请注意，某些组件可以有自己的子域，例如 Container Registry，同样也需要这些子域的证书。）

## Installation and Configuration

These configuration settings are commonly used when configuring Omnibus GitLab.
For a complete list of settings, see the [README](../README.md#configuring) file.

- [安装极狐GitLab](https://about.gitlab.cn/install/)。
  - [手动下载安装包进行安装](../manual_install.md)。
- 为实例 [设置域名/URL](../settings/configuration.md#configuring-the-external-url-for-gitlab) 以便于访问。
- [启用 HTTPS](../settings/nginx.md#enable-https)。
- [启用通知邮件](../settings/smtp.md#smtp-settings)。
- [启用通过电子邮件回复](https://docs.gitlab.com/ee/administration/reply_by_email.html#set-it-up)。
  - [安装并配置 postfix](https://docs.gitlab.com/ee/administration/reply_by_email_postfix_setup.html)。
- [启用容器仓库](https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-domain-configuration)。
  - 容器仓库的域名需要 SSL 证书。
- [启用 GitLab Pages](https://docs.gitlab.com/ee/administration/pages/)。
  - 如果您想要启用 HTTPS，您必须获得通配证书。
- [启用 Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html)。

<!--
- [GitLab Mattermost](../gitlab-mattermost/README.md) Set up the Mattermost messaging app that ships with Omnibus GitLab package.
- [GitLab Prometheus](https://docs.gitlab.com/ee/administration/monitoring/performance/prometheus.html)
  Set up the Prometheus monitoring included in the Omnibus GitLab package.
- [GitLab High Availability Roles](../roles/README.md).
-->

<!--
## Using Docker image

You can also use the Docker images provided by GitLab to install and configure a GitLab instance.
Check the [documentation](../docker/README.md) to know more.
-->
