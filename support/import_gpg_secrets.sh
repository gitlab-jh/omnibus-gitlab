#!/bin/bash
# GPG key for package signing
if [ -n "$SECRET_AWS_ACCESS_KEY_ID" ]; then
  echo -e "[default]\naws_access_key_id = $AWS_ACCESS_KEY_ID \naws_secret_access_key = $AWS_SECRET_ACCESS_KEY" > ~/.aws/config
  AWS_ACCESS_KEY_ID="$SECRET_AWS_ACCESS_KEY_ID" AWS_SECRET_ACCESS_KEY="$SECRET_AWS_SECRET_ACCESS_KEY" aws s3 cp s3://omnibus-sig/package.sig.key .
  gpg --batch --no-tty --allow-secret-key-import --import package.sig.key
  rm package.sig.key
elif [ -n "$QCLOUD_COS_SECRET_ID" ]; then
  CurrentTimeStamp=$( date '+%s' )
  ExpiredTimeStamp=$[ $CurrentTimeStamp + 60 ]
  COSHost="https://gpg-1303695223.cos.ap-hongkong.myqcloud.com"
  PrivateKeyFileName="package.sig.key"
  KeyTime="$CurrentTimeStamp;$ExpiredTimeStamp"
  SignKey=$( echo -n $KeyTime | openssl dgst -sha1 -hmac $QCLOUD_COS_SECRET_KEY | sed 's/(stdin)= //g' )
  HttpString="get\\n/$PrivateKeyFileName\\n\\n\\n"
  HttpStringSHA1=$( echo -e -n $HttpString | openssl dgst -sha1 | sed 's/(stdin)= //g' )
  StringToSign="sha1\\n$KeyTime\\n$HttpStringSHA1\\n"
  Signature=$( echo -e -n $StringToSign | openssl dgst -sha1 -hmac $SignKey | sed 's/(stdin)= //g' )
  URL="$COSHost/$PrivateKeyFileName?q-sign-algorithm=sha1&q-ak=$QCLOUD_COS_SECRET_ID&q-sign-time=$KeyTime&q-key-time=$KeyTime&q-header-list=&q-url-param-list=&q-signature=$Signature"
  curl $URL --output $PrivateKeyFileName
  gpg --batch --no-tty --allow-secret-key-import --import $PrivateKeyFileName
  rm $PrivateKeyFileName
else
  echo "No GPG secret key were imported."
fi
