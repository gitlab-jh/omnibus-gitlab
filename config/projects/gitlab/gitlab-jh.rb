name 'gitlab-jh'
description 'GitLab JH Edition (including NGINX, Postgres, Redis)'
maintainer 'JiHu(GitLab) <support@gitlab.cn>'
homepage 'https://about.gitlab.cn/'

override :graphicsmagick , source: { url: "https://cfhcable.dl.sourceforge.net/project/graphicsmagick/graphicsmagick/1.3.36/GraphicsMagick-1.3.36.tar.gz" }
override :pcre, source: { url: "https://cfhcable.dl.sourceforge.net/project/pcre/pcre/8.44/pcre-8.44.tar.gz" }
override :pcre2, source: { url: "https://cfhcable.dl.sourceforge.net/project/pcre/pcre2/10.34/pcre2-10.34.tar.gz" }
override :'pkg-config-lite', source: { url: "https://cfhcable.dl.sourceforge.net/project/pkgconfiglite/0.28-1/pkg-config-lite-0.28-1.tar.gz" }
override :liblzma, source: { url: "https://cfhcable.dl.sourceforge.net/project/lzmautils/xz-5.2.4.tar.gz" }
override :libxml2, source: { url: "https://devops-omnibus-softwares-1303695223.cos.na-siliconvalley.myqcloud.com/libxml2-2.9.10.tar.gz" }
override :libxslt, source: { url: "https://devops-omnibus-softwares-1303695223.cos.na-siliconvalley.myqcloud.com/libxslt-1.1.32.tar.gz" }