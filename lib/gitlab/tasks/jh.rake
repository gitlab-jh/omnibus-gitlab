require 'fileutils'
require_relative "../build.rb"
require_relative "../build/info.rb"
require_relative '../build/omnibus_trigger'
require_relative "../ohai_helper.rb"
require_relative '../version.rb'
require_relative "../util.rb"
require 'net/http'
require 'json'

namespace :jh do
  namespace :package do
    desc "Sync packages to tencent cos"
    task :sync do
      release_bucket = Gitlab::Util.get_env('JH_BUCKET')
      release_bucket_region = Gitlab::Util.get_env('JH_BUCKET_REGION')
      release_s3_endpoint = 'cos.ap-shanghai.myqcloud.com'
      # packages in cache are under directory `pkg/<os_name>-<os_version>/`
      dirs = Dir.glob('pkg/**').select { |f| File.directory? f}
      dirs.each do |dir|
        os_info = dir.split('/')[1].split('-')
        os_name = os_info[0]
        os_version = os_info[1]
        dest = "#{os_name}/#{os_version}"
        system(*%W[aws s3 --endpoint-url https://#{release_s3_endpoint} sync #{dir} s3://#{release_bucket}/#{dest} --no-progress --acl public-read --region #{release_bucket_region}])
        files = Dir.glob("#{dir}/*").select { |f| File.file? f }
        files.each do |file|
          puts file.gsub(dir, "https://#{release_bucket}.#{release_s3_endpoint}/#{dest}").gsub('+', '%2B')
        end
      end
    end
    desc "Push Urls cache after package release"
    task :push_cdn_cache do
      # TODO: tccli should be avialable in the container
      system(*%w[apt-get update -q])
      system(*%w[apt-get install -yq python3-pip])
      system(*%w[pip3 install tccli])
      cdn_domain = "omnibus.gitlab.cn"
      dirs = Dir.glob('pkg/**').select { |f| File.directory? f}
      dirs.each do |dir|
        os_info = dir.split('/')[1].split('-')
        os_name = os_info[0]
        os_version = os_info[1]
        dest = "#{os_name}/#{os_version}"
        files = Dir.glob("#{dir}/*").select { |f| File.file? f }
        target_urls = Array.new
        files.each do |file|
          target_urls.push(file.gsub(dir, "https://#{cdn_domain}/#{dest}").gsub('+', '%2B'))
        end
        command = %w[tccli cdn PushUrlsCache --cli-unfold-argument --Urls]
        command.concat(target_urls)
        system(*command)
      end
    end

  end

  namespace :docker do
    desc "Release docker image"
    task push: ["docker:pull:staging"] do
      final_tag = Gitlab::Util.get_env('IMAGE_TAG') || Build::Info.release_version.sub(/(\.|-)jh\.\d*$/, '')
      final_image = "#{Gitlab::Util.get_env('JH_REGISTRY')}/gitlab-jh"
      DockerOperations.authenticate(Gitlab::Util.get_env('JH_REGISTRY_USER'), Gitlab::Util.get_env('JH_REGISTRY_PASSWORD'), Gitlab::Util.get_env('JH_REGISTRY'))
      DockerOperations.tag_and_push(
        Build::GitlabImage.gitlab_registry_image_address,
        final_image,
        Build::Info.docker_tag,
        final_tag
      )
      puts "Pushed #{final_image}:#{final_tag}"
    end

    desc "Release latest docker image"
    task push_latest: ["docker:pull:staging"] do
      final_image = "#{Gitlab::Util.get_env('JH_REGISTRY')}/gitlab-jh"
      DockerOperations.authenticate(Gitlab::Util.get_env('JH_REGISTRY_USER'), Gitlab::Util.get_env('JH_REGISTRY_PASSWORD'), Gitlab::Util.get_env('JH_REGISTRY'))
      DockerOperations.tag_and_push(
        Build::GitlabImage.gitlab_registry_image_address,
        final_image,
        Build::Info.docker_tag,
        "latest"
      )
      puts "Pushed #{final_image}:latest"
    end
  end
end
